﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T9Spelling
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Enter number of cases(or type 'exit' to exit):");
                string input = Console.ReadLine();
                if (input == "exit")
                {
                    break;
                }
                else
                {
                    int numberOfCases;
                    int.TryParse(input, out numberOfCases);
                    ReadData(numberOfCases);
                }
               
            }                
        }

        private static void ReadData(int numberOfCases)
        {
                     
            if (numberOfCases == 0)
            {
                return;
            }
            string[] inputData = new string[numberOfCases];

            for (int i = 0; i < numberOfCases; i++)
            {
                inputData[i] = Console.ReadLine();
            }

            string[] result = T9.GetOutput(inputData);

            for (int i = 0; i < numberOfCases; i++)
            {
                Console.WriteLine(string.Format("Case #{0}:" + result[i], i + 1));
            }
            return;
        }
    }
}
