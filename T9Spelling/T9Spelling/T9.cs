﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace T9Spelling
{
    public static class T9
    {
        private static readonly Regex Validator = new Regex(@"^[a-z\s]*$");

        private static bool IsValid(string str)
        {
            return Validator.IsMatch(str);
        }


        public static string[] GetOutput(string[] data)
        {
            string[] result = new string[data.Length];
            for (int i = 0; i < data.Length; i++)
            {
                result[i] = IsValid(data[i]) ?
                     T9.Convert(data[i]) :
                    " bad input, only a-z allowed";
            }
            return result;
        }

        private static string Convert(string dataRow)
        {
            string result = "";
            foreach (char c in dataRow)
            {
                if (char.IsWhiteSpace(c))
                {
                    AddToResult("0", ref result);
                }
                else
                {
                    int letterNum = (c - char.Parse("a"));
                    int times;
                    int button;
                    switch (letterNum)
                    {
                        case 18:
                            button = 7;
                            times = 4;
                            break;
                        case 25:
                            button = 9;
                            times = 4;
                            break;
                        default:
                            if (letterNum > 18)
                            {
                                times = 1 + (letterNum + 2) % 3;
                                button = 1 + (letterNum + 2) / 3;
                            }
                            else
                            {
                                times = 1 + (letterNum + 3) % 3;
                                button = 1 + (letterNum + 3) / 3;
                            }
                            break;
                    }

                    string res = "";
                    for (int i = 0; i < times; i++)
                    {
                        res += button.ToString();
                    }
                    AddToResult(res, ref result);
                }
            }

            return result;
        }

        private static void AddToResult(string num, ref string result)
        {
            if (result.Length == 0)
            {
                result = num;
            }
            else
            {
                result = num.First() == result.Last() ? result + " " + num : result + num;
            }         
        }
    }
}
