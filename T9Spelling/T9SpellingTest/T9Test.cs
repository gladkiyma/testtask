﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using T9Spelling;

namespace T9SpellingTest
{
    [TestClass]
    public class T9Test
    {
        [TestMethod]
        public void TestLetters() {
            string[] data = new string[] {"a","b","d", "k", "s", "t", "v", "w", "y", "z" };
            string[] result = T9.GetOutput(data);
            Assert.IsTrue(result[0]=="2","a");
            Assert.IsTrue(result[1] == "22","b");
            Assert.IsTrue(result[2] == "3", "d");
            Assert.IsTrue(result[3] == "55", "k");
            Assert.IsTrue(result[4] == "7777", "s");
            Assert.IsTrue(result[5] == "8", "t");
            Assert.IsTrue(result[6] == "888", "v");
            Assert.IsTrue(result[7] == "9", "w");
            Assert.IsTrue(result[8] == "999", "y");
            Assert.IsTrue(result[9] == "9999", "z");
        }

        [TestMethod]
        public void TestFromTask()
        {
            string[] data = new string[] { "hi", "yes", "foo  bar", "hello world"};
            string[] result = T9.GetOutput(data);
            Assert.IsTrue(result[0] == "44 444", "hi");
            Assert.IsTrue(result[1] == "999337777", "yes");
            Assert.IsTrue(result[2] == "333666 6660 022 2777", "foo  bar");
            Assert.IsTrue(result[3] == "4433555 555666096667775553", "hello world");
         
        }

    }
}
